<?php

/**
 * Contains \Drupal\webpay_payment\Plugin\Payment\Method\Webpay.
 */

namespace Drupal\webpay_payment\Plugin\Payment\Method;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\payment\Entity\PaymentInterface;
use Drupal\payment\Plugin\Payment\LineItem\PaymentLineItemInterface;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimple;
use Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodOffsiteSimpleInterface;

/**
 * A webpay payment method.
 *
 * Plugins that extend this class must have the following keys in their plugin
 * definitions:
 * - entity_id: (string) The ID of the payment method entity the plugin is for.
 * - execute_status_id: (string) The ID of the payment status plugin to set at
 *   payment execution.
 *
 * @PaymentMethod(
 *   id = "payment_webpay",
 *   deriver = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteDeriver",
 *   operations_provider = "\Drupal\payment_offsite_api\Plugin\Payment\Method\PaymentMethodBaseOffsiteOperationsProvider"
 * )
 */
class Webpay extends PaymentMethodOffsiteSimple implements PaymentMethodOffsiteSimpleInterface {

  /**
   * Webpay server payment URL.
   */
  const SERVER_URL = 'https://payment.webpay.by';
  const TEST_SERVER_URL = 'https://securesandbox.webpay.by';

  /**
   * {@inheritdoc}
   */
  public function getSupportedCurrencies() {
    return TRUE;
  }

  /**
   * Gets the ID of the payment method this plugin is for.
   *
   * @return string
   */
  public function getEntityId() {
    return $this->pluginDefinition['entity_id'];
  }

  public function getServer() {
    return $this->pluginDefinition['config']['server'];
  }

  public function getAction() {
    return $this->getServer() == 'test' ? self::TEST_SERVER_URL : self::SERVER_URL;
  }

  public function getStoreId() {
    return $this->pluginDefinition['config']['wsb_storeid'];
  }

  public function getSecretKey() {
    return $this->pluginDefinition['config']['secret_key'];
  }


  /**
   * {@inheritdoc}
   */
  public function updatePaymentStatusAccess(AccountInterface $account) {
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getSettablePaymentStatuses(AccountInterface $account, PaymentInterface $payment) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getResultPages() {
    return [
      'success' => TRUE,
      'fail' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function paymentForm() {
    $form = array();
    $payment = $this->getPayment();
    $form['#action'] = $this->getAction();

    $url = Url::fromRoute('webpay.notify', [], ['absolute' => TRUE]);

    dpm($payment->getLineItems(), '$payment->getLineItems()');
    dpm(round($payment->getAmount(), 2), 'getAmount');
    dpm($this->getSignature(), 'getSignature');
    $this->addPaymentFormData('*scart', '');
    $this->addPaymentFormData('wsb_version', 2);
    $this->addPaymentFormData('wsb_language_id', 'russian');
    $this->addPaymentFormData('wsb_storeid', $this->getStoreId());
    //wsb_store
    $this->addPaymentFormData('wsb_order_num', $this->getOrderNum());
    $wsb_test = ($this->getServer() == 'test') ? '1': '0';
    $this->addPaymentFormData('wsb_test', $wsb_test);
    $this->addPaymentFormData('wsb_currency_id', $payment->getCurrencyCode());

    $seed = $this->getSeed();
    $this->addPaymentFormData('wsb_seed', $seed);
    $this->addPaymentFormData('wsb_signature', $seed);
    /** @var PaymentLineItemInterface[] $lineItems */
    $lineItems = $payment->getLineItems();

    $index = 0;
    foreach ($lineItems as $key => $lineItem) {
      //$this->addPaymentFormData("wsb_invoice_item_name_$key", $lineItem->getName());
      $this->addPaymentFormData("wsb_invoice_item_name[$index]", $lineItem->getDescription());
      $this->addPaymentFormData("wsb_invoice_item_quantity[$index]", $lineItem->getQuantity());
      $this->addPaymentFormData("wsb_invoice_item_price[$index]", $this->amountFormat($lineItem->getTotalAmount()));
      $index++;
    }
    // wsb_notify_url
    $this->addPaymentFormData('wsb_notify_url', $url->toString());
    // wsb_return_url
    $this->addPaymentFormData('wsb_return_url', $url->toString());
    // wsb_cancel_return_url
    $this->addPaymentFormData('wsb_cancel_return_url', $url->toString());

    $this->addPaymentFormData('wsb_total', $this->amountFormat($payment->getAmount()));
    $this->addPaymentFormData('wsb_signature', $this->getSignature());

    $form += $this->generateForm();

    return $form;
  }

  public function amountFormat($amount) {
    return number_format(round($amount, 2), 2, '.', '');
  }

  public function getOrderNum() {
    return 'ORDER-' . $this->getPayment()->id();
  }

  /**
   * {@inheritdoc}
   *
   * @todo refactor
   */
  public function getSignature($signature_type = PaymentMethodOffsiteSimple::SIGN_OUT) {
    $payment = $this->getPayment();
    $signature_data = array(
      $payment->getCreatedTime(),
      $this->getStoreId(),
      $this->getOrderNum(),
      ($this->getServer() == 'test') ? '1': '0',
      $payment->getCurrency()->getCurrencyCode(),
      (string) $this->amountFormat(round($payment->getAmount(), 2)),
      $this->getSecretKey(),
    );

    // Calculate signature.
    return hash('sha1', implode('', $signature_data));
  }

  /**
   * Return local payment status related to given remote.
   *
   * @param string $status
   *   Remote status id.
   *
   * @return string.
   *   Local status id.
   */
  public function getStatusId($status) {
    return $this->pluginDefinition['ipn_statuses'][$status];
  }

  /**
   * {@inheritdoc}
   */
  public function ipnExecute() {
    if (!$this->ipnValidate()) {
      return [
        'status' => 'fail',
        'message' => 'bad data',
        'response_code' => 200,
      ];
    }

    $inv_id = $this->request->request->get('InvId');
    $payment_status = $this->getStatusId($inv_id);
    $status = isset($payment_status) ? $payment_status : 'payment_pending';
    $this->getPayment()->setPaymentStatus($this->paymentStatusManager->createInstance($status));
    $this->getPayment()->save();

    return [
      'status' => 'success',
      'message' => 'OK' . $this->getPayment()->id(),
      'response_code' => 200,
    ];
  }

  /**
   * Returns success message.
   *
   * @return array
   *   The renderable array.
   */
  public function getSuccessContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', array('@external_status' => 'success')),
    ];
  }

  /**
   * Returns fail message.
   *
   * @return array
   *   The renderable array.
   */
  public function getFailContent() {
    return [
      '#markup' => $this->t('Payment processed as @external_status.', array('@external_status' => 'FAIL')),
    ];
  }

  public function getSeed() {
    return $this->getPayment()->getCreatedTime();
  }
  /**
   * {@inheritdoc}
   */
  public function getMerchantIdName() {
    return 'MrchLogin';
  }

  /**
   * {@inheritdoc}
   */
  public function getTransactionIdName() {
    return 'InvId';
  }

  /**
   * {@inheritdoc}
   */
  public function getAmountName() {
    return 'OutSum';
  }

  /**
   * {@inheritdoc}
   */
  public function getSignatureName() {
    return 'SignatureValue';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeys() {
    $required_keys = [
      'OutSum',
      'InvId',
    ];
    if (!$this->isFallbackMode()) {
      $required_keys[] = 'SignatureValue';
    }

    return $required_keys;
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured() {
    return !empty($this->getAction());
  }
}
